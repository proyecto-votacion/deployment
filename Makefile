all:	api web compose

api:
	docker load < api.tar
web:
	docker load < web.tar
compose:
	docker compose up
