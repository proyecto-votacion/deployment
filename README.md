# deployment



## Getting started

### How to load api and web images

```
docker load < api.tar
docker load < web.tar
````

### How to run docker compose
```
docker compose up
````
### How to run the demo
```
make all
````
